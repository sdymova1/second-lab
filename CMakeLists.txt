cmake_minimum_required(VERSION 3.17)
project(SecondLabOOP)

set(CMAKE_CXX_STANDARD 14)

add_executable(SecondLabOOP main.cpp)