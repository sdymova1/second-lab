#include <iostream>
#include <string>
#include <utility>

int base = 10; //notation

// Written by Sophie Dymova, K-20

using namespace std;

class LongNumbers;

class Multi {
public:
    virtual LongNumbers mult(LongNumbers a, LongNumbers b) = 0;
};

class Primality {
public:
    virtual bool prim(long long a, long long i) = 0;
};

class LongNumbers {
public:
    string value;
    static Multi *mult_way;
    static Primality *primality_test;

    LongNumbers(string x) {
        value = move(x);
    }

    LongNumbers() {
        value = "0";
    }

    static long equalizer(string &num1, string &num2) {
        long n = max(num1.size(), num2.size());

        while (num1.size() < n)
            num1.insert(0, "0");

        while (num2.size() < n)
            num2.insert(0, "0");

        return n;
    }

    static string add(string num1, string num2) {
        int spare = 0;
        long n = equalizer(num1, num2);
        string res;
        int sum;

        for (int i = n - 1; i >= 0; i--) {
            sum = (num1[i] - '0') + (num2[i] - '0') + spare;
            res.insert(0, to_string(sum % base));
            spare = sum / base;
        }

        if (spare) {
            res.insert(0, to_string(spare));
        }

        return res.erase(0, min(res.find_first_not_of('0'), res.size() - 1));
    }

    static string subtract(string num1, string num2) {
        int difference;
        long n = equalizer(num1, num2);
        string x, y, res;

        if (num1 > num2) {
            x = num1;
            y = num2;
        } else {
            x = num2;
            y = num1;
        }

        for (int i = n - 1; i >= 0; i--) {
            difference = (x[i] - '0') - (y[i] - '0');

            if (difference >= 0)
                res.insert(0, to_string(difference));

            else {
                int prev = i - 1;
                while (prev >= 0) {
                    x[prev] = (base + (x[prev] - '0') - 1) % base + '0';

                    if (x[prev] != '9')
                        break;
                    else
                        prev--;
                }
                res.insert(0, to_string(difference + base));
            }
        }
        return res.erase(0, min(res.find_first_not_of('0'), res.size() - 1));
    }

    static string x10_multiplier(string &num, long times) {
        for (int k = 0; k < times; k++)
            num.append("0");
        return num;
    }

    static long gcd(long a, long b) {
        if (a == 0)
            return b;

        else if (b == 0 | a == b)
            return a;

        else if (a > b)
            return gcd(a - b, b);

        return gcd(a, b - a);
    }

    static long long power(long long a, long long m, long long mod) {
        long long res = 1;
        a = a % mod;

        while (m > 0) {
            if (m % 2)
                res = (res * a) % mod;

            a = (a * a) % mod;
            m /= 2;
        }
        return res % mod;
    }

    LongNumbers &operator=(string x) {
        *this = LongNumbers(move(x));
        return *this;
    }

    LongNumbers operator+(const LongNumbers &x) {
        return LongNumbers(add((*this).value, x.value));
    }

    LongNumbers operator-(const LongNumbers &x) {
        return LongNumbers(subtract((*this).value, x.value));
    }

    LongNumbers operator*(const LongNumbers &x) {
        return mult_way->mult((*this).value, x.value);
    }

    static bool test(long long a, long long i) {
        return primality_test -> prim(a, i);
    }
};

long double reciprocal(long num, long double x0) {
    return x0 * (2 - num * x0);
}

class Karatsuba : public Multi {
public:
    Karatsuba() = default;

    LongNumbers mult(LongNumbers a, LongNumbers b) override {
        LongNumbers res, a0, a1, b0, b1, m1, m2, Z1;
        long n = LongNumbers::equalizer(a.value, b.value);

        if (n == 1) {
            return to_string((a.value[0] - '0') * (b.value[0] - '0'));
        }

        a0 = a.value.substr(0, n / 2);
        a1 = a.value.substr(n / 2, n - n / 2);
        b0 = b.value.substr(0, n / 2);
        b1 = b.value.substr(n / 2, n - n / 2);

        m1 = a0 * b0;
        m2 = a1 * b1;
        Z1 = (a0 + a1) * (b0 + b1) - m1 - m2;

        LongNumbers::x10_multiplier(m1.value, 2 * (n - n / 2));
        LongNumbers::x10_multiplier(Z1.value, (n - n / 2));

        res = m1 + m2 + Z1;

        return res.value.erase(0, min(res.value.find_first_not_of('0'), res.value.size() - 1));
    }
};


class Toom3 : public Multi {
public:
    Toom3() = default;

    LongNumbers mult(LongNumbers a, LongNumbers b) override {
        LongNumbers res, a0, a1, a2, b0, b1, b2, m1, m2, m0, m01, m02, m12;
        long l = LongNumbers::equalizer(a.value, b.value);

        if (l == 1) {
            return res = to_string((stoi(a.value.substr(0, l))) * (stoi(b.value.substr(0, l))));

        }

        if (l % 3) {
            l += 3 - l % 3;
            while (a.value.size() < l)
                a.value.insert(0, "0");
            LongNumbers::equalizer(a.value, b.value);
        }

        a0 = a.value.substr(0, l / 3);
        a1 = a.value.substr(l / 3, l / 3);
        a2 = a.value.substr(2 * l / 3, l / 3);
        b0 = b.value.substr(0, l / 3);
        b1 = b.value.substr(l / 3, l / 3);
        b2 = b.value.substr(2 * l / 3, l / 3);

        m0 = a0 * b0;
        m1 = a1 * b1;
        m2 = a2 * b2;

        m01 = (a0 + a1) * (b0 + b1) - m0 - m1;
        m02 = (a0 + a2) * (b0 + b2) - m0 - m2;
        m12 = (a1 + a2) * (b1 + b2) - m1 - m2;

        m0 = LongNumbers::x10_multiplier(m0.value, 4 * l / 3);
        m01 = LongNumbers::x10_multiplier(m01.value, 3 * l / 3);
        m1 = LongNumbers::x10_multiplier(m1.value, 2 * l / 3);
        m02 = LongNumbers::x10_multiplier(m02.value, 2 * l / 3);
        m12 = LongNumbers::x10_multiplier(m12.value, l / 3);

        res = m0 + m1 + m2 + m01 + m02 + m12;

        return res.value.erase(0, min(res.value.find_first_not_of('0'), res.value.size() - 1));
    }
};

class M_R : public Primality {
public:
    M_R() = default;

    bool prim(long long n, long long k) override {

        if (n != 2 && !(n % 2)) {
            return false;
        }

        long long m = n - 1;

        while (!(m % 2)) {
            m /= 2;
        }

        for (int i = 0; i < k; i++)
        {
            long long a = rand() % n;

            long long u = LongNumbers::power(a, m, n);

            while (m != n - 1 && u != 1 && u != n - 1) {
                u = LongNumbers::power(u, 2, n);
                m *= 2;
            }

            if (u != n - 1 && !(m % 2)) {
                return false;
            }
        }
        return true;
    }
};

Multi *LongNumbers::mult_way = new Karatsuba();
Primality *LongNumbers::primality_test = new M_R();

int main() {
    LongNumbers a, b;
    a = "23423456783456789098765234567898765432345678987654323456784323457654321234567876543234567567891004";
    b = "98234234567876543234567898765432123456789987654356456765434565434565434565434565456545431234567878123456789876543212345678909876543902";

    LongNumbers res = a * b;
    cout << res.value << "\n" << endl;

    long long numb = 827;
    if (LongNumbers::test(numb, 5))
        cout << numb << " is prime";
    else {cout << numb << " is NOT prime";}

    /*
    long double x0 = 0.1;
    for (int i = 0; i < 10; i++) {
        x0 = reciprocal(19, x0);
    }
    cout << int(x0);
    */
    return 0;
}
